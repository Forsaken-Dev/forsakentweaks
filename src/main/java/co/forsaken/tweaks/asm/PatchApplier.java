package co.forsaken.tweaks.asm;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;

import net.minecraft.launchwrapper.IClassTransformer;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.util.CheckClassAdapter;
import org.objectweb.asm.util.TraceClassVisitor;

import co.forsaken.tweaks.api.IPatch;

import com.google.common.collect.HashMultimap;

import cpw.mods.fml.common.FMLLog;

public class PatchApplier implements IClassTransformer {
  private static final File            _debugFolder = new File("debug").getAbsoluteFile();
  private HashMultimap<String, IPatch> _patches     = HashMultimap.create();
  private static PatchApplier          _instance;

  public PatchApplier() {
    _instance = this;
  }

  public static PatchApplier instance() {
    return _instance;
  }

  public void addPatch(IPatch patch) {
    for (String className : patch.getClassNames()) {
      _patches.put(className, patch);
    }
  }

  public byte[] transform(String name, String transformedName, byte[] bytes) {
    if (EngineCorePlugin.isEnabled()) {
      if (_patches.containsKey(name)) {
        Set<IPatch> patches = _patches.get(name);

        FMLLog.fine("[ForsakenTweaks] Found %s patches for \"%s\"", patches.size(), name);

        ClassNode classNode = new ClassNode();
        ClassReader classReader = new ClassReader(bytes);

        classReader.accept(classNode, ClassReader.EXPAND_FRAMES);

        for (IPatch patch : patches) {
          patch.transform(name, classNode);
        }

        try {
          ClassWriter classWriter = new ClassWriter(ClassWriter.COMPUTE_MAXS);

          ClassVisitor visitor = classWriter;

          PrintWriter printWriter = null;

          if (EngineCorePlugin.isDebug()) {
            if (!_debugFolder.exists()) {
              _debugFolder.mkdirs();
            }

            printWriter = new PrintWriter(new FileWriter(new File(_debugFolder, name.replace('/', '.') + ".log")));

            visitor = new TraceClassVisitor(visitor, printWriter);
            visitor = new CheckClassAdapter(visitor);
          }

          classNode.accept(visitor);

          if (printWriter != null) {
            printWriter.close();
          }

          bytes = classWriter.toByteArray();

          if (EngineCorePlugin.isDebug()) {
            FileOutputStream output = new FileOutputStream(new File(_debugFolder, name.replace('/', '.') + ".class"));

            output.write(bytes);

            output.flush();
            output.close();
          }
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }

    return bytes;
  }

}
