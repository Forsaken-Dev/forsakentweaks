package co.forsaken.tweaks.api;

import org.objectweb.asm.tree.ClassNode;

import co.forsaken.tweaks.common.Configuration;

public abstract class IPatch {
  private boolean _enabled = true;

  public abstract String[] getClassNames();

  public void loadConfigurations(Configuration configuration) {
    _enabled = configuration.get("mod." + getClassNames()[0], "enabled", _enabled).getBoolean(_enabled);
  }

  public void transform(String name, ClassNode node) {
    if (!_enabled) { return; }
    for (String s : getClassNames()) {
      if (s.equalsIgnoreCase(name)) {
        patch(node);
        return;
      }
    }
  }

  protected abstract void patch(ClassNode node);
}
