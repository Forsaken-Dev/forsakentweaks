package co.forsaken.tweaks.common;

import java.util.ArrayList;

public class Property {
  public enum Type {
    STRING,
    INTEGER,
    BOOLEAN,
    DOUBLE;

    private static Type[] values = { STRING, INTEGER, BOOLEAN, DOUBLE };

    public static Type tryParse(char id) {
      for (int x = 0; x < values.length; x++) {
        if (values[x].getID() == id) { return values[x]; }
      }

      return STRING;
    }

    public char getID() {
      return name().charAt(0);
    }
  }

  private String        name;
  public String         value;
  public String         comment;
  public String[]       valueList;

  private final boolean wasRead;
  private final boolean isList;
  private final Type    type;

  public Property() {
    wasRead = false;
    type = null;
    isList = false;
  }

  public Property(String name, String value, Type type) {
    this(name, value, type, false);
  }

  Property(String name, String value, Type type, boolean read) {
    setName(name);
    this.value = value;
    this.type = type;
    wasRead = read;
    isList = false;
  }

  public Property(String name, String[] values, Type type) {
    this(name, values, type, false);
  }

  Property(String name, String[] values, Type type, boolean read) {
    setName(name);
    this.type = type;
    valueList = values;
    wasRead = read;
    isList = true;
  }

  public int getInt() {
    return getInt(-1);
  }

  public int getInt(int _default) {
    try {
      return Integer.parseInt(value);
    } catch (NumberFormatException e) {
      return _default;
    }
  }

  public boolean isIntValue() {
    try {
      Integer.parseInt(value);
      return true;
    } catch (NumberFormatException e) {
      return false;
    }
  }

  public boolean getBoolean(boolean _default) {
    if (isBooleanValue()) {
      return Boolean.parseBoolean(value);
    } else {
      return _default;
    }
  }

  public boolean isBooleanValue() {
    return ("true".equals(value.toLowerCase()) || "false".equals(value.toLowerCase()));
  }

  public boolean isDoubleValue() {
    try {
      Double.parseDouble(value);
      return true;
    } catch (NumberFormatException e) {
      return false;
    }
  }

  public double getDouble(double _default) {
    try {
      return Double.parseDouble(value);
    } catch (NumberFormatException e) {
      return _default;
    }
  }

  public int[] getIntList() {
    ArrayList<Integer> nums = new ArrayList<Integer>();

    for (String value : valueList) {
      try {
        nums.add(Integer.parseInt(value));
      } catch (NumberFormatException e) {
      }
    }

    int[] primitives = new int[nums.size()];

    for (int i = 0; i < nums.size(); i++) {
      primitives[i] = nums.get(i);
    }

    return primitives;
  }

  public boolean isIntList() {
    for (String value : valueList) {
      try {
        Integer.parseInt(value);
      } catch (NumberFormatException e) {
        return false;
      }
    }
    return true;
  }

  public boolean[] getBooleanList() {
    ArrayList<Boolean> values = new ArrayList<Boolean>();
    for (String value : valueList) {
      try {
        values.add(Boolean.parseBoolean(value));
      } catch (NumberFormatException e) {
      }
    }

    boolean[] primitives = new boolean[values.size()];

    for (int i = 0; i < values.size(); i++) {
      primitives[i] = values.get(i);
    }

    return primitives;
  }

  public boolean isBooleanList() {
    for (String value : valueList) {
      if (!"true".equalsIgnoreCase(value) && !"false".equalsIgnoreCase(value)) { return false; }
    }

    return true;
  }

  public double[] getDoubleList() {
    ArrayList<Double> values = new ArrayList<Double>();
    for (String value : valueList) {
      try {
        values.add(Double.parseDouble(value));
      } catch (NumberFormatException e) {
      }
    }

    double[] primitives = new double[values.size()];

    for (int i = 0; i < values.size(); i++) {
      primitives[i] = values.get(i);
    }

    return primitives;
  }

  public boolean isDoubleList() {
    for (String value : valueList) {
      try {
        Double.parseDouble(value);
      } catch (NumberFormatException e) {
        return false;
      }
    }

    return true;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public boolean wasRead() {
    return wasRead;
  }

  public Type getType() {
    return type;
  }

  public boolean isList() {
    return isList;
  }
}