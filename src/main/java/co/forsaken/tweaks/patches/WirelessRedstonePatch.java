package co.forsaken.tweaks.patches;

import static org.objectweb.asm.Opcodes.ALOAD;
import static org.objectweb.asm.Opcodes.F_APPEND;
import static org.objectweb.asm.Opcodes.GETFIELD;
import static org.objectweb.asm.Opcodes.IFNONNULL;
import static org.objectweb.asm.Opcodes.INTEGER;
import static org.objectweb.asm.Opcodes.RETURN;

import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.FrameNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.VarInsnNode;

import co.forsaken.tweaks.api.IPatch;
import co.forsaken.tweaks.asm.util.ClassUtil;
import cpw.mods.fml.common.FMLLog;

public class WirelessRedstonePatch extends IPatch {

  @Override
  public String[] getClassNames() {
    return new String[] { "codechicken.wirelessredstone.core.RedstoneEtherServer" };
  }

  @Override
  protected void patch(ClassNode node) {
    MethodNode method = ClassUtil.getMethod(node, "updateJammedNodes", String.format("(Labw;)V"));
    if (method == null) { return; }

    InsnList toInject = new InsnList();
    LabelNode label = new LabelNode();

    toInject.add(new JumpInsnNode(IFNONNULL, label));
    toInject.add(new InsnNode(RETURN));
    toInject.add(label);
    toInject.add(new FrameNode(F_APPEND, 1, new Object[] { INTEGER }, 0, null));
    toInject.add(new VarInsnNode(ALOAD, 0)); // this
    toInject.add(new FieldInsnNode(GETFIELD, "codechicken/wirelessredstone/core/RedstoneEtherServer", "ethers", "Ljava/util/HashMap;"));

    method.instructions.insert(toInject);
    FMLLog.info("[ForsakenTweaks] Patched %s sucessfully", "WR-CBE::updatedJammedNodes");
  }
}
